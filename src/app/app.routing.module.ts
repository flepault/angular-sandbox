import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SandBoxComponent } from './layout/sand-box/sand-box.component';

const homeRoute = ''
const sandboxRoute = 'sandbox';

const routes: Routes = [
    {path: homeRoute, component: SandBoxComponent},
    {path: sandboxRoute, component: SandBoxComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
