import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SandBoxComponent } from './sand-box/sand-box.component';
import { DropZoneComponent } from "./drop-zone/drop-zone.component";

@NgModule({
    imports: [SharedModule],
    declarations: [DropZoneComponent, SandBoxComponent, DropZoneComponent],
    exports: [DropZoneComponent, SandBoxComponent]
})
export class LayoutModule {
}
