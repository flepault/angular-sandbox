import { HttpErrorResponse, HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { DropzoneService } from "../../shared/service/dropzone/dropzone.service";
import { DropZoneFileDTO } from "../../shared/model/drop-zone-file.model";

export interface FileUpload {
    file: File;
    inProgress: boolean;
    progress: number;
}

@Component({
    selector: 'app-drop-zone',
    templateUrl: './drop-zone.component.html'
})
export class DropZoneComponent {

    @ViewChild('fileUpload', {static: false})
    fileUpload: ElementRef;
    files = [];

    constructor(private dropzoneService: DropzoneService) {
    }

    private uploadFile(fileUpload: FileUpload): void {

        const file: FormData = new FormData();
        file.append(
            fileUpload.file.name,
            fileUpload.file,
            fileUpload.file.name
        );

        fileUpload.inProgress = true;

        this.dropzoneService.upload(file).pipe(
            map((event: HttpEvent<DropZoneFileDTO>) => {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        fileUpload.progress = Math.round(event.loaded * 100 / event.total);
                        break;
                    case HttpEventType.Response:
                        return event;
                }
            }),
            catchError((error: HttpErrorResponse) => {
                fileUpload.inProgress = false;
                return of(`${fileUpload.file.name} upload failed: ${error}`);
            })).subscribe((event: any) => {
            if (typeof (event) === 'object') {
                console.log('Upload');
                return;
            }
        });
    }

    private uploadFiles() {
        this.fileUpload.nativeElement.value = '';
        this.files.forEach(file => {
            this.uploadFile(file);
        });
    }

    public onClick(): void {
        const fileUpload = this.fileUpload.nativeElement;
        fileUpload.onchange = () => {
            for (let i = 0, len = fileUpload.files.length; i < len; i++) {
                this.files.push({file: fileUpload.files[i], inProgress: false, progress: 0});
            }
            this.uploadFiles();
        };
        fileUpload.click();
    }
}
