import { TestBed } from '@angular/core/testing';
import { SharedModule } from '../../shared/shared.module';
import { SandBoxComponent } from './sand-box.component';


describe('SandBoxComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule],
            declarations: [SandBoxComponent]
        }).compileComponents();
    });

    it('should create the app', () => {
        const fixture = TestBed.createComponent(SandBoxComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
