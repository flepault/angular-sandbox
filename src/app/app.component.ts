import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { ResponsiveService } from './shared/service/responsive/responsive.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-angular-sandbox',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    public isProd: boolean;

    menu = false;

    isMobile$: Observable<boolean>;

    constructor(private router: Router,
                public responsiveService: ResponsiveService) {
        this.isProd = environment.production;
        this.isMobile$ = responsiveService.isMobile();
    }

    ngOnInit(): void {
        this.router.events.subscribe(event => {
            this.closeMenu();
        });
    }

    openMenu(): void {
        this.menu = true;
    }

    closeMenu(): void {
        this.menu = false;
    }

}
