export interface DropZoneFile {
    name: String;
}

export interface DropZoneFileDTO {
    name: String;
}