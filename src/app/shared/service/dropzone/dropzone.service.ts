import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { DropZoneFile } from "../../model/drop-zone-file.model";

@Injectable({
    providedIn: 'root'
})
export class DropzoneService {

    constructor(private http: HttpClient) {
    }

    // UPLOAD IMAGE GALLERIE
    public upload(file: FormData): Observable<HttpEvent<DropZoneFile>> {
        return this.http.post<DropZoneFile>(environment.dropzoneURL + '/upload',
            file,
            {
                reportProgress: true,
                observe: 'events'
            });
    }

    // GET ALL IMAGE URL
    public download(): Observable<DropZoneFile[]> {
        return this.http.get<DropZoneFile[]>(environment.dropzoneURL + '/get', {});
    }

}
