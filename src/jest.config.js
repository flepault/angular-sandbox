module.exports = {
    verbose: true,
    collectCoverage: true,
    collectCoverageFrom: ["src/**/*.ts"],
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/setupJest.ts'],
    globals: {
        'ts-jest': {
            tsConfig: '<rootDir>/src/tsconfig.spec.json'
        }
    }
}
